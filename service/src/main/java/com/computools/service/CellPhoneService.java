package com.computools.service;

import com.mvv.data.audit.dao.CellPhonesRepo;
import com.mvv.data.audit.model.CellPhone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CellPhoneService extends BaseService<CellPhone, CellPhonesRepo> {

    @Autowired
    public CellPhoneService(CellPhonesRepo dao) {
        super(dao);
    }
}
