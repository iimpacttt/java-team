package com.computools.service;

import com.mvv.data.audit.model.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public class BaseService<T extends BaseEntity, S extends JpaRepository<T , UUID>> {

    protected S dao;

    public T save(T item){
        return dao.save(item);
    }
    public void saveAll(List<T> items){
        dao.saveAll(items);
    }

    public void delete(T item){
        dao.delete(item);
    }

    public void delete(UUID itemID){
        dao.deleteById(itemID);
    }

    public T getById(UUID id){
        return dao.getOne(id);
    }

    public List<T> getAll(){
        return dao.findAll();
    }

    public Page<T> getAll(Pageable pageable){
        return dao.findAll(pageable);
    }

    @Autowired
    public BaseService(S dao) {
        this.dao = dao;
    }
}
