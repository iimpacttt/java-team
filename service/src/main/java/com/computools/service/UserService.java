package com.computools.service;

import com.mvv.data.audit.dao.CompUserRepo;
import com.mvv.data.audit.model.CompUser;
import com.computools.dto.sign.SignInDTO;
import com.computools.dto.sign.SignInResponseDTO;
import com.mvv.security.jwt.service.JWTService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;

@Service
@Transactional
public class UserService extends BaseService<CompUser, CompUserRepo> {
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;
    private final ModelMapper modelMapper;

    public CompUser getByEmail(String email){
        return dao.getByEmail(email).orElseThrow(RuntimeException::new);
    }

    public CompUser updateOrCreateUser(CompUser user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (user.getId() != null){
            user = mapEntity(user);
        }
        dao.save(user);
        return user;
    }

    public CompUser mapEntity(CompUser user) {
        CompUser existUser = dao.findById(user.getId()).orElseThrow(() -> new RuntimeException("Cant get item"));
        modelMapper.map(user, existUser);
        return existUser;
    }

    public SignInResponseDTO signIn(SignInDTO signInDTO) throws UnsupportedEncodingException {
        CompUser compUser = getByEmail(signInDTO.getEmail());
        if (compUser == null) throw new RuntimeException("Invalid email");
        if (passwordEncoder.matches(signInDTO.getPassword(), compUser.getPassword())){
            return new SignInResponseDTO(jwtService.generateToken(compUser));
        }
        throw new RuntimeException("Invalid password");
    }

    @Autowired
    public UserService(CompUserRepo dao,
                       PasswordEncoder passwordEncoder, JWTService jwtService,
                       ModelMapper modelMapper) {
        super(dao);
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.modelMapper = modelMapper;
    }
}
