package com.computools.dto.database;

public class CellPhoneDTO extends BaseDTO {
    private String model;
    private String manufacturedBy;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturedBy() {
        return manufacturedBy;
    }

    public void setManufacturedBy(String manufacturedBy) {
        this.manufacturedBy = manufacturedBy;
    }
}
