package com.computools.dto;

import org.springframework.data.domain.Sort;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

public class ListRequestDTO {
    @NotNull
    @Max(50)
    private Integer limit;

    @NotNull
    private Integer offset;

    private Sort sort = Sort.unsorted();

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
