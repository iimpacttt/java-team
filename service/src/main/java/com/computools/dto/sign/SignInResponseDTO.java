package com.computools.dto.sign;

public class SignInResponseDTO {
    private final String token;

    public String getToken() {
        return token;
    }

    public SignInResponseDTO(String token) {
        this.token = token;
    }
}
