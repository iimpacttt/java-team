package com.mvv.web.configuration;

import com.mvv.data.audit.config.DatabaseConfiguration;
import com.mvv.data.audit.holders.ObjectMapperHolder;
import com.mvv.data.audit.model.CellPhone;
import com.mvv.data.audit.model.CompUser;
import com.mvv.security.jwt.configuration.JWTSecurityConfig;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@Import({DatabaseConfiguration.class, JWTSecurityConfig.class})
@EnableAutoConfiguration
@EnableWebMvc
@ComponentScan(basePackages = {"com.computools.service", "com.computools.web.controller"})
public class AppConfig {

    @Bean
    @Primary
    public ObjectMapper objectMapper(){
        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Bean
    public ModelMapper modelMapper(){
       ModelMapper modelMapper = new ModelMapper();
       modelMapper.addMappings(new PropertyMap<CompUser, CompUser>() {
           @Override
           protected void configure() {
               skip().setCreatedTime(null);
           }
       });
       modelMapper.addMappings(new PropertyMap<CellPhone, CellPhone>() {
           @Override
           protected void configure() {
               skip().setCreatedTime(null);
           }
       });
       return modelMapper;
    }

    @Bean
    public ObjectMapperHolder objectMapperHolder(){
        return new ObjectMapperHolder(objectMapper());
    }
}
