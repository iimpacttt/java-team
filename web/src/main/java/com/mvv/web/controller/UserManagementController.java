package com.mvv.web.controller;

import com.mvv.data.audit.model.CompUser;
import com.computools.dto.database.UserDTO;
import com.computools.dto.sign.SignInDTO;
import com.computools.dto.sign.SignInResponseDTO;
import com.computools.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
public class UserManagementController {

    private final UserService userService;
    private final ModelMapper modelMapper;

    @GetMapping
    public String init(){
        return "HEllo";
    }

    @PostMapping("/sign-in")
    public ResponseEntity<SignInResponseDTO> signIn(@Validated @RequestBody SignInDTO signInDTO) throws UnsupportedEncodingException {
        return ResponseEntity.ok(userService.signIn(signInDTO));
    }

    @PostMapping("/sign-up")
    public ResponseEntity signUp(@Validated @RequestBody UserDTO userDTO) {
        userService.updateOrCreateUser(modelMapper.map(userDTO, CompUser.class));
        return ResponseEntity.accepted().build();
    }

    @Autowired
    public UserManagementController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }
}
