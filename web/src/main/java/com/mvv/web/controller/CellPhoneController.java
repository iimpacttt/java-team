package com.mvv.web.controller;

import com.mvv.data.audit.model.CellPhone;
import com.computools.dto.database.CellPhoneDTO;
import com.computools.service.CellPhoneService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.mvv.web.controller.BaseController.BASE_SECURED_URL;
import static com.mvv.web.controller.CellPhoneController.CELL_PHONE_URL;

@RestController
@RequestMapping(path = BASE_SECURED_URL + CELL_PHONE_URL)
public class CellPhoneController extends BaseController<CellPhoneDTO, CellPhoneService, CellPhone>{
    public static final String CELL_PHONE_URL = "/phone";

    @Autowired
    public CellPhoneController(CellPhoneService service, ModelMapper mapper) {
        super(service, mapper, CellPhoneDTO.class, CellPhone.class);
    }
}
