package com.mvv.web.controller;

import com.mvv.data.audit.model.CompUser;
import com.computools.dto.database.UserDTO;
import com.computools.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.mvv.web.controller.BaseController.BASE_SECURED_URL;
import static com.mvv.web.controller.UserController.USER_CONTROLLER_MAPPING;

@RestController
@RequestMapping(path = BASE_SECURED_URL + USER_CONTROLLER_MAPPING)
public class UserController extends BaseController<UserDTO, UserService, CompUser> {

    public static final String USER_CONTROLLER_MAPPING = "/user";

    @Override
    public ResponseEntity createItem(@Validated @RequestBody UserDTO request)  {
        return new ResponseEntity<>(
                service.updateOrCreateUser(mapper.map(request, entityType)), HttpStatus.OK);
    }

    @Autowired
    public UserController(UserService service, ModelMapper modelMapper) {
        super(service, modelMapper, UserDTO.class, CompUser.class);
    }
}
