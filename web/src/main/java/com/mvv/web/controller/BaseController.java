package com.mvv.web.controller;

import com.mvv.data.audit.model.BaseEntity;
import com.computools.dto.ListRequestDTO;
import com.computools.dto.database.BaseDTO;
import com.computools.service.BaseService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.stream.Collectors;


public class BaseController<T extends BaseDTO, S extends BaseService<R, ?>, R extends BaseEntity> {
    public static final String BASE_SECURED_URL = "/team/secured";
    protected ModelMapper mapper;

    protected S service;

    protected Class<T> type;

    protected Class<R> entityType;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<T> getById(@PathVariable UUID itemId){
        return ResponseEntity.ok(mapper.map(service.getById(itemId), type));
    }

    @GetMapping(path = "/list")
    public ResponseEntity<Page<T>> getList(@Validated ListRequestDTO requestDTO){
        Pageable request = PageRequest.of(requestDTO.getOffset(),
                requestDTO.getLimit(), requestDTO.getSort());
        Page<?> entites = service.getAll(request);

        Page<T> response = new PageImpl<>(entites.stream().map(item -> mapper.map(item, type)).collect(Collectors.toList()),
                request, entites.getTotalElements());
        return ResponseEntity.ok(response);
    }

    @Deprecated
    @PostMapping
    public ResponseEntity createItem(@Validated @RequestBody T request) {
        service.save(mapper.map(request, entityType));
        return ResponseEntity.accepted().build();
    }

    @DeleteMapping
    public ResponseEntity removeItem(@PathVariable UUID itemId){
        service.delete(itemId);
        return ResponseEntity.accepted().build();
    }

    public BaseController(S service, ModelMapper mapper,
                          Class<T> type, Class<R> entityType) {
        this.service = service;
        this.mapper = mapper;
        this.type = type;
        this.entityType = entityType;
    }
}
