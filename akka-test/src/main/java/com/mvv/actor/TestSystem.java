package com.mvv.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.io.IOException;

public class TestSystem {
    public static void main(String[] args) {
        ActorSystem actorSystem = ActorSystem.create("testSystem");

        ActorRef compActor = actorSystem.actorOf(Props.create(ComputoolsActor.class), "computools-actor");
        ActorRef compActor1 = actorSystem.actorOf(Props.create(ComputoolsActor.class), "computools-actor1");
        long start = System.currentTimeMillis();
        compActor.tell("testMessage", ActorRef.noSender());
        compActor.tell("message", ActorRef.noSender());
        compActor1.tell("message", ActorRef.noSender());

        System.out.println("Time spend for actor working -> " + (System.currentTimeMillis() - start));
        System.out.println("Press ENTER to exit");

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            actorSystem.terminate();
        }
    }
}
