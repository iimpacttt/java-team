package com.mvv.actor;

import akka.actor.AbstractActor;

public class ComputoolsActor extends AbstractActor {

    @Override
    public Receive createReceive() {
        return receiveBuilder().matchEquals("message", p -> {
            System.out.println("GET MESSAGE, THREAD -> " + Thread.currentThread().getName());
            System.out.println(getContext().self());
            System.out.println(

            );
        })
                .matchEquals("testMessage", p -> {
                    Thread.sleep(1000);
                    System.out.println("GET TEST MESSAGE, THREAD -> " + Thread.currentThread().getName());
                    System.out.println(getContext().self());
                    System.out.println();
                }).build();
    }
}
