package com.mvv.messaging.server;

import org.springframework.web.socket.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CopyOnWriteArrayList;

public class WebSocketServerHandler implements WebSocketHandler {
    private final CopyOnWriteArrayList<WebSocketSession> sessions = new CopyOnWriteArrayList();

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws Exception {
        sessions.add(webSocketSession);
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        sessions.stream().filter(item -> !item.equals(webSocketSession)).forEach(session -> {
            try {
                session.sendMessage(webSocketMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        throwable.printStackTrace();

    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        webSocketSession.close(closeStatus);
    }

    @Override
    public boolean supportsPartialMessages() {
        return true;
    }

    public void sendTestFile(WebSocketSession webSocketSession) throws IOException {
        byte[] file = Files.readAllBytes(Paths.get("/home/finch/Downloads/Telegram Desktop/video_2018-07-07_18-01-09.mp4"));
        int offset = 0;
        boolean isLast = false;
        int length = 8196;


        while (offset < file.length){
            if ((file.length - offset) <= length) {
                length = file.length - offset;
                isLast = true;
            }
            byte[] buffer = new byte[length];
            System.arraycopy(file, offset, buffer, 0, length);

            webSocketSession.sendMessage(new BinaryMessage(ByteBuffer.wrap(buffer), isLast));
            offset += length;
        }
    }

}
