package com.mvv.messaging.configuration;


import com.mvv.messaging.server.WebSocketServerHandler;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.computools.messaging.bootstrap"})
public class WebSocketConfigure implements WebSocketConfigurer {


    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(serverSocket(), "/websocket").setAllowedOrigins("*");
    }

    @Bean
    public WebSocketServerHandler serverSocket(){
        return new WebSocketServerHandler();
    }

}