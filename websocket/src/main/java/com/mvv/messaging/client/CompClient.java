package com.mvv.messaging.client;

import org.springframework.web.socket.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CompClient implements WebSocketHandler {

    private final int clientNumber;
    private final String TEMP_FOLDER = "/home/victor_manzar/video/";
    private final String message = String.format("{\"messageId\":\"1\", \"cameraId\":\"%s\", \"trackId\":\"0\"}", "rpENjcOqQWTfkjSYeWkpvsDSc");
    private final String message1= String.format("{\"messageId\":\"1\", \"cameraId\":\"%s\", \"trackId\":\"1\"}", "zmuHNgJqZzIUnaULFudHvMDMo");
    private final String message2 = String.format("{\"messageId\":\"1\", \"cameraId\":\"%s\", \"trackId\":\"2\"}", "ZZaLkQylUpFMhggdTBsaqJraw");
    private final String message3 = String.format("{\"messageId\":\"1\", \"cameraId\":\"%s\", \"trackId\":\"3\"}", "QlbpgsLPlHGdECAQkrIJXByAr");

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("Engine");
        session.setBinaryMessageSizeLimit(10_000_000);
//        session.sendMessage(new TextMessage("Hello message " + clientNumber));
//        session.sendMessage(new TextMessage(message));
//        session.sendMessage(new TextMessage(message1));
//        session.sendMessage(new TextMessage(message2));
//        session.sendMessage(new TextMessage(message3));
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        if (message instanceof TextMessage) {
            Thread.sleep(1000);
            System.out.println( "Client " + clientNumber + " receive message " + ((TextMessage) message).getPayload());

//            session.sendMessage(new TextMessage("Message from client " + clientNumber));
        }
        if (message instanceof BinaryMessage){
            BinaryMessage binaryMessage = (BinaryMessage) message;
            System.out.println(String.format("Current message size is -> %d, Current track id -> %d,",
                    binaryMessage.getPayloadLength(), binaryMessage.getPayload().array()[0]));

            saveFile(binaryMessage, binaryMessage.getPayload().array()[0]);
        }
    }

//
  private void saveFile(BinaryMessage binaryMessage, byte trackID){
        File folder = new File(TEMP_FOLDER + clientNumber);

        if (!folder.exists()){
            folder.mkdir();
        }

        File file = new File( TEMP_FOLDER + clientNumber + "/"+ "Track" + trackID + System.currentTimeMillis() + ".mp4");

        try (OutputStream outputStream = new FileOutputStream(file)) {
            if (!file.exists()){
                file.createNewFile();
            }
            outputStream.write(binaryMessage.getPayload().array(), 1, binaryMessage.getPayloadLength() -  1);
        } catch (IOException e) {
            e.printStackTrace();
        }
  }
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        System.out.println("Transport error");
        exception.printStackTrace();
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {

    }

    @Override
    public boolean supportsPartialMessages() {
        return true;
    }

    public CompClient(int clientNumber) {
        this.clientNumber = clientNumber;
    }
}
