package com.mvv.messaging;

import com.mvv.messaging.configuration.WebSocketConfigure;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class WebSocketStarter {
    public static void main(String[] args) {
        new SpringApplicationBuilder(WebSocketConfigure.class)
                .web(WebApplicationType.NONE)
                .build().run(args);
    }
}
