package com.mvv.messaging.bootstrap;

import com.mvv.messaging.client.CompClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.jetty.JettyWebSocketClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ClientStarter implements CommandLineRunner {

    List<JettyWebSocketClient> clients = new ArrayList<>();

    @Override
    public void run(String... strings) throws Exception {
        initJettyClient(1);
        Thread.sleep(1000 * 60 * 10);
        clients.forEach(JettyWebSocketClient::stop);
    }

    private void initJettyClient(int clientNumber) throws URISyntaxException{
        for (int i =0 ; i < clientNumber; i++) {
            JettyWebSocketClient client = new JettyWebSocketClient();
            CompClient compClient = new CompClient(i);
            client.start();
//        client.doHandshake(compClient, null, new URI("ws://35.242.220.3:8080/api/stream"));
            client.doHandshake(compClient, null, new URI("ws://localhost:8080/api/stream"));
            clients.add(client);
        }
    }
}
