package com.mvv.reactive.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.config.EnableWebFlux;

@Configuration
@EnableWebFlux
@ComponentScan(basePackages = {"com.computools.reactive.web.controller"})
@EnableAutoConfiguration
public class ReactiveConfig {
    @Bean
    @Primary
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }
}
