package com.mvv.reactive.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/location")
public class LocationFluxController extends BaseController {

    @GetMapping
    public Mono<Response> hello(){
        CompletableFuture<Response> future = CompletableFuture.supplyAsync(Response::new);
        Mono<Response> mono = Mono.fromFuture(future);
//        return ServerResponse.ok().body(mono, Response.class);
        return mono;
    }

    private final class Response{
        public String name = "Hello";
        public String lastName = "Man";
    }
}
