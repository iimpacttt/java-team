package com.computools.data.cascade.dto;

import com.computools.data.cascade.domain.Employee;

import java.util.Set;


public class ProjectDto {

    private Long id;

    private String projectName;

    private Set<EmployeeDto> employees;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Set<EmployeeDto> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<EmployeeDto> employees) {
        this.employees = employees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}