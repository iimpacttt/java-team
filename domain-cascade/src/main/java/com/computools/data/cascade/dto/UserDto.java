package com.computools.data.cascade.dto;

import java.util.Set;

public class UserDto {

    private Long id;
    private String userName;
    private PassportDto passport;
    private Set<AddressDto> addressDto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public PassportDto getPassport() {
        return passport;
    }

    public void setPassport(PassportDto passport) {
        this.passport = passport;
    }

    public Set<AddressDto> getAddressDto() {
        return addressDto;
    }

    public void setAddressDto(Set<AddressDto> addressDto) {
        this.addressDto = addressDto;
    }
}
