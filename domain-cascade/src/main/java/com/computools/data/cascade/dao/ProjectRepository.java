package com.computools.data.cascade.dao;

import com.computools.data.cascade.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query(nativeQuery = true, value = "delete from employee_project where project_is = :pr_id")
    void deleteReletions(@Param("pr_id") Long projectId);
}
