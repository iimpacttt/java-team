package com.computools.data.cascade.service;

import com.computools.data.cascade.dao.AddressRepository;
import com.computools.data.cascade.dao.UserRepository;
import com.computools.data.cascade.domain.Address;
import com.computools.data.cascade.domain.UserDefault;
import com.computools.data.cascade.domain.converter.ObjectMapperHolder;
import com.computools.data.cascade.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    public UserDefault createUser(UserDto userDto) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        UserDefault user;
        user = ObjectMapperHolder.getObjectMapper().convertValue(userDto, UserDefault.class);
        userRepository.save(user);
        //--------------------------------------------------
        List<Address> userAddresses = new ArrayList<>();
        if(userDto.getAddressDto() != null){
            userDto.getAddressDto().forEach((addressDto -> {
                try {
                    Address address = ObjectMapperHolder.getObjectMapper().convertValue(addressDto, Address.class);
                    address.setUser(user);
                    userAddresses.add(address);
                    //addressRepository.save(address);
                } catch (ObjectMapperHolder.ObjectMapperHolderNotInitialized objectMapperHolderNotInitialized) {
                    objectMapperHolderNotInitialized.printStackTrace();
                }
            }));
        }
        //-------------------------------------------------------
        user.setAddresses(new HashSet<>(userAddresses));
        return user;

    }


    public UserDefault updateUser(UserDto userDto) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        UserDefault user;

        user = ObjectMapperHolder.getObjectMapper().convertValue(userDto, UserDefault.class);
        Set<Address> newAdresses = new HashSet<>();
        //----------------------------------------------------------------------------------------------------------------------
        if(userDto.getAddressDto() != null){
            userDto.getAddressDto().forEach((addressDto -> {
                try {
                    Address address = ObjectMapperHolder.getObjectMapper().convertValue(addressDto, Address.class);
                    if(address.getId() != null)
                        newAdresses.add(address);
                    else {
                        address.setUser(user);
                        newAdresses.add(addressRepository.save(address));
                    }
                } catch (ObjectMapperHolder.ObjectMapperHolderNotInitialized objectMapperHolderNotInitialized) {
                    objectMapperHolderNotInitialized.printStackTrace();
                }
            }));
        }
        user.setAddresses(newAdresses);
        //--------------------------------------------------------------------------------------------------------------------
        if(user.getPassport() != null && user.getPassport().getId() == null)
            user.getPassport().setUser(user);
        return userRepository.save(user);
    }

    public List<UserDefault> getAll(){
        return userRepository.findAll();
    }

    public void delete(Long id) {
        UserDefault userToDelete = userRepository.getOne(id);
        userRepository.delete(userToDelete);
    }

}
