package com.computools.data.cascade.dto;

import java.util.HashSet;
import java.util.Set;

public class EmployeeDto {

    private Long id;
    private String name;
    private Set<ProjectDto> projects;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ProjectDto> getProjects() {
        if(projects == null)
            projects = new HashSet<>();
        return projects;
    }

    public void setProjects(Set<ProjectDto> projects) {
        this.projects = projects;
    }
}