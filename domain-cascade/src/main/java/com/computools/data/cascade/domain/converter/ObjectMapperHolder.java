package com.computools.data.cascade.domain.converter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperHolder {
    private static volatile ObjectMapper objectMapper;

    public ObjectMapperHolder(ObjectMapper objectMapper) {
        ObjectMapperHolder.objectMapper = objectMapper;
    }

    public static ObjectMapper getObjectMapper() throws ObjectMapperHolderNotInitialized {
        if (objectMapper == null) {
            synchronized(ObjectMapperHolder.class) {
                if (objectMapper == null) {
                    throw new ObjectMapperHolder.ObjectMapperHolderNotInitialized();
                }
            }
        }

        return objectMapper;
    }

    public static class ObjectMapperHolderNotInitialized extends Exception {
        private static final long serialVersionUID = 8923602000912259794L;

        public ObjectMapperHolderNotInitialized() {
            super("Holder not initialized by Spring container");
        }
    }
}

