package com.computools.data.cascade.dao;

import com.computools.data.cascade.domain.UserDefault;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDefault, Long> {
}
