package com.computools.data.cascade.domain.converter;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public abstract class Abstract2StringAttributeConverter<X> implements AttributeConverter<X, String> {
    protected static final Supplier<Object> DEFAULT_NULL_RESOLVER = () -> null;
    protected final Supplier<X> defaultValueSupplier;

    protected Abstract2StringAttributeConverter(Supplier<X> nullResolver) {
        this.defaultValueSupplier = requireNonNull(nullResolver);
    }

    @SuppressWarnings("unchecked")
    public Abstract2StringAttributeConverter() {
        this((Supplier<X>) DEFAULT_NULL_RESOLVER);
    }

    @Override
    public String convertToDatabaseColumn(X attribute) {
        if (attribute == null) {
            return null;
        }
        return toDataBase(attribute);
    }

    @Override
    public X convertToEntityAttribute(String dbData) {
        if (isEmptyDbData(dbData)) {
            return defaultValueSupplier.get();
        }

        return fromDataBase(dbData);
    }

    protected boolean isEmptyDbData(String dbData) {
        return StringUtils.isEmpty(dbData);
    }

    protected abstract String toDataBase(X attribute);

    protected abstract X fromDataBase(String dbData);
}
