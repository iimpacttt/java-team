package com.computools.data.cascade.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "passports")
public class Passport extends BaseEntity{


    @Column
    private String number;


    //One to one -----------------------------
    //1, 2 - Связь один-к-одному с использованием явного внешнего ключа
    @JsonIgnore
    @OneToOne(mappedBy = "passport", fetch = FetchType.EAGER)
    private UserDefault user;
    //----------------------------------------


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public UserDefault getUser() {
        return user;
    }

    public void setUser(UserDefault user) {
        this.user = user;
    }
}