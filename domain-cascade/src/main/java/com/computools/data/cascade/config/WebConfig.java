package com.computools.data.cascade.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.computools.data.cascade.web"})
public class WebConfig {

}
