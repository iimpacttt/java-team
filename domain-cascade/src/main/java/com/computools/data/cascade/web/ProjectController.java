package com.computools.data.cascade.web;

import com.computools.data.cascade.domain.Project;
import com.computools.data.cascade.domain.converter.ObjectMapperHolder;
import com.computools.data.cascade.domain.converter.ProjectToDtoConverter;
import com.computools.data.cascade.dto.ProjectDto;
import com.computools.data.cascade.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectToDtoConverter projectToDtoConverter;

    @PostMapping
    public ResponseEntity<ProjectDto> createProject(@RequestBody ProjectDto project) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        return ResponseEntity.ok(projectToDtoConverter.projectToDto(projectService.createProject(project)));
    }

    @PutMapping
    public ResponseEntity<ProjectDto> updateProject(@RequestBody ProjectDto project) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        return ResponseEntity.ok(projectToDtoConverter.projectToDto(projectService.updateProject(project)));
    }

    @GetMapping
    public ResponseEntity<List<ProjectDto>> getAll(){
        return ResponseEntity.ok(projectToDtoConverter.projectToDto(projectService.getAll()));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("id") Long id){
        projectService.delete(id);
    }
}
