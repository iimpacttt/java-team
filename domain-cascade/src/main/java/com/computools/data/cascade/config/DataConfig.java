package com.computools.data.cascade.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.computools.data.cascade.dao")
@EntityScan(
        value = "com.computools.data.cascade.domain",
        basePackageClasses = Jsr310JpaConverters.class
)
public class DataConfig {
}
