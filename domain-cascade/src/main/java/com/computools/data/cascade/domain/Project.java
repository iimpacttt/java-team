package com.computools.data.cascade.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Project")
public class Project extends  BaseEntity{

    @Column
    private String projectName;

    @ManyToMany(mappedBy = "projects", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Employee> employees = new HashSet<>();

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Set<Employee> getEmployees() {
        if(employees == null)
            employees = new HashSet<>();
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    /*@PreRemove
    private void preRemove(){
        if(employees != null)
            employees.forEach(empl-> {
                if(empl.getProjects() != null)
                empl.getProjects().remove(this);
            });
    }*/


    @Override
    public int hashCode() {
        int result;
        if(id == null)
            result = 17;
        else result = Integer.parseInt(id.toString());
        result = 31 * result + projectName.hashCode();
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Project) {
            return id.equals(((Project) obj).getId()) && projectName.equals(((Project) obj).getProjectName());
        }
        return false;
    }
}