package com.computools.data.cascade.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

@Entity
public class Address extends BaseEntity{

    @Column
    private String city;

    @Column
    private String street;

    @Column
    private String building;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToOne
    @JoinColumn(nullable = true, name = "user_id")
    private UserDefault user;

    public UserDefault getUser() {
        return user;
    }

    public void setUser(UserDefault user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }
}
