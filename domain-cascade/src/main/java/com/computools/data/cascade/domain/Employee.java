package com.computools.data.cascade.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Employee")
public class Employee extends BaseEntity{

    @Column
    private String name;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE })
    @JoinTable(
            name = "Employee_Project",
            joinColumns = { @JoinColumn(name = "employee_id") },
            inverseJoinColumns = { @JoinColumn(name = "project_id") }
    )
    private Set<Project> projects = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Project> getProjects() {
        if(projects == null)
            projects = new HashSet<>();
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        int result;
        if(id == null)
            result = 17;
        else result = Integer.parseInt(id.toString());
        result = 31 * result + name.hashCode();
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Employee) {
            return id.equals(((Employee) obj).getId()) && name.equals(((Employee) obj).getName());
        }
        return false;
    }
}