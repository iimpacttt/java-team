package com.computools.data.cascade.domain;

import com.computools.data.cascade.domain.converter.Set2StringConverter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users_default")
public class UserDefault extends BaseEntity{

    @Column
    private String userName;

    //One to one-----------------------------

    //1 - Связь один-к-одному с использованием явного внешнего ключа
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinColumn(name="passport_id")
    private Passport passport;

    //2 - использование 3 таблицы. Такой себе вараинт. Не рекомендую
    /*@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinTable(name = "user_passport",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name="passport_id")
    )
    private Passport passport;*/
    //---------------------------------------
    //orphanRemoval - allow cascade delete
    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH},
                                fetch = FetchType.EAGER/*, orphanRemoval = true*/)
    private Set<Address> addresses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public Set<Address> getAddresses() {
        if(addresses == null)
            addresses = new HashSet<>();
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}