package com.computools.data.cascade;

import com.computools.data.cascade.config.DomainCascadeConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class DomainCascadeStarter {
    public static void main(String[] args) {
        new SpringApplicationBuilder(DomainCascadeConfig.class).run(args);
    }
}
