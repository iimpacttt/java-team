package com.computools.data.cascade.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan(basePackages = {"com.computools.data.cascade.service"})
@EnableAsync
@EnableScheduling
public class ServiceConfig {

}
