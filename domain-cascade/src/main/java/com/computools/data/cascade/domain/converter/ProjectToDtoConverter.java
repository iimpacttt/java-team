package com.computools.data.cascade.domain.converter;

import com.computools.data.cascade.domain.Project;
import com.computools.data.cascade.dto.EmployeeDto;
import com.computools.data.cascade.dto.ProjectDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class ProjectToDtoConverter {

    public ProjectDto projectToDto(Project project){
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setProjectName(project.getProjectName());
        if(project.getEmployees() != null){
            projectDto.setEmployees(new HashSet<>());
            project.getEmployees().forEach(empl->{
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.setName(empl.getName());
                employeeDto.setId(empl.getId());
                projectDto.getEmployees().add(employeeDto);
            });
        }
        return projectDto;
    }

    public List<ProjectDto> projectToDto(List<Project> projects){
        List<ProjectDto> result = new ArrayList();
        projects.forEach(project -> result.add(projectToDto(project)));
        return result;
    }

}
