package com.computools.data.cascade.service;

import com.computools.data.cascade.dao.EmployeeRepository;
import com.computools.data.cascade.dao.ProjectRepository;
import com.computools.data.cascade.domain.Employee;
import com.computools.data.cascade.domain.Project;
import com.computools.data.cascade.domain.converter.ObjectMapperHolder;
import com.computools.data.cascade.dto.EmployeeDto;
import com.computools.data.cascade.dto.ProjectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private EmployeeRepository employeeRepository;


    public Project createProject(ProjectDto projectDto) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        Set<Employee> existingEmployee = new HashSet<>();
        if(projectDto.getEmployees() != null && !projectDto.getEmployees().isEmpty()){
            existingEmployee.addAll(employeeRepository.getByIdIn(projectDto.getEmployees().stream()
                    .map(EmployeeDto::getId).collect(Collectors.toList())));
        }//-----------------------------------------------------------------------------------
        Project project;
        project = ObjectMapperHolder.getObjectMapper().convertValue(projectDto, Project.class);
        project.getEmployees().removeIf(empl-> empl.getId() != null);

        project.getEmployees().addAll(existingEmployee);
        projectRepository.save(project);

        //-------------------------------------------------------------------------------------------
        if (project.getEmployees() != null)
            project.getEmployees().stream().forEach((empl) ->
            {
                if (empl.getId() != null) {
                    employeeRepository.getOne(empl.getId()).getProjects().add(project);
                } else empl.getProjects().add(project);
            });

        return project;

    }

    public List<Project> getAll(){
        return projectRepository.findAll();
    }

    public void delete(Long id) {
        Project project = projectRepository.getOne(id);

        projectRepository.deleteReletions(id);
        projectRepository.delete(project);
    }

    public Project updateProject(ProjectDto projectDto) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        Project project = projectRepository.getOne(projectDto.getId());

        if(project.getEmployees() != null) {
            Project finalProject = project;
            project.getEmployees().forEach(employee -> employee.getProjects().remove(finalProject));
        }

        project = ObjectMapperHolder.getObjectMapper().convertValue(projectDto, Project.class);

        Project finalProject = project;
        project.getEmployees().forEach(empl-> empl.getProjects().add(finalProject));
       projectRepository.save(project);
        return project;

    }
}
