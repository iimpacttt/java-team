package com.computools.data.cascade.web;

import com.computools.data.cascade.domain.UserDefault;
import com.computools.data.cascade.domain.converter.ObjectMapperHolder;
import com.computools.data.cascade.dto.UserDto;
import com.computools.data.cascade.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<UserDefault> createUser(@RequestBody UserDto user) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        return ResponseEntity.ok(userService.createUser(user));
    }

    @PutMapping
    public ResponseEntity<UserDefault> updateUser(@RequestBody UserDto user) throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        return ResponseEntity.ok(userService.updateUser(user));
    }

    @GetMapping
    public ResponseEntity<List<UserDefault>> getAll(){
        return ResponseEntity.ok(userService.getAll());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("id") Long id){
        userService.delete(id);
    }
}
