package com.mvv.security.jwt.provider;

import com.mvv.security.jwt.service.JWTService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.cache.NullUserCache;

public class JWTProvider extends AbstractUserDetailsAuthenticationProvider {

    private final JWTService userService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
//        NOT NECESSARY
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        String token = (String) authentication.getPrincipal();
        return userService.getUserDetailsFromToken(token);
    }

    public JWTProvider(JWTService userService) {
        this.userService = userService;
        this.setUserCache(new NullUserCache());
    }
}
