package com.mvv.security.jwt.service;

import com.mvv.data.audit.model.CompUser;
import com.mvv.data.audit.details.CompUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;

@Service
public class JWTService {
    private final String secret;
    private final int tokenValidHours;

    public String generateToken(CompUser compUser) throws UnsupportedEncodingException {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.HOUR, tokenValidHours);

        Map<String, Object> claims = new HashMap<>();
        claims.put("id", compUser.getId());
        claims.put("roles", compUser.getRoles());

        return Jwts.builder().setSubject("Computools education token")
                .setExpiration(calendar.getTime())
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret.getBytes("UTF-8"))
                .compact();
    }

    @SuppressWarnings("unchecked")
    public UserDetails getUserDetailsFromToken(String token){
        Jws<Claims> userDetailsParts = parseToken(token);
        CompUser compUser = new CompUser();
        compUser.setId(UUID.fromString(userDetailsParts.getBody().get("id", String.class)));
        compUser.setRoles(new HashSet<>(userDetailsParts.getBody().get("roles", ArrayList.class)));

        return new CompUserDetails(compUser);
    }

    private Jws<Claims> parseToken(String token) {
        Jws<Claims> result = null;
        try {
            result = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token);
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("Cant parse token", e);
        }

        return result;
    }


    public JWTService(@Value("${jwt.secret}") String secret,
                      @Value("${jwt.tokenValidHours}") int tokenValidHours) {
        this.secret = secret;
        this.tokenValidHours = tokenValidHours;
    }
}
