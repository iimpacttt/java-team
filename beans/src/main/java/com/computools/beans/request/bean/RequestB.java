package com.computools.beans.request.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST
        , proxyMode = ScopedProxyMode.TARGET_CLASS
)
public class RequestB {

    public void doJob(){
    }

    public RequestB(){
        System.out.println("Created new instance of RequestB");
    }
}
