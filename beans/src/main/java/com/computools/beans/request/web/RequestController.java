package com.computools.beans.request.web;

import com.computools.beans.request.bean.RequestB;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/request")
public class RequestController {
    private final RequestB requestB;

    @GetMapping
    public void getBean(){
        requestB.doJob();
        requestB.doJob();
        requestB.doJob();
    }


    public RequestController(RequestB requestB) {
        this.requestB = requestB;
    }
}
