package com.computools.beans.session.web;

import com.computools.beans.session.bean.SessionB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/session")
public class SessionController {
    private final SessionB sessionB;

    @GetMapping
    public void doJob(){
        sessionB.doJob();
    }

    @Autowired
    public SessionController(SessionB sessionB) {
        this.sessionB = sessionB;
    }
}
