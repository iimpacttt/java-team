package com.computools.beans.prototype.service;

import com.computools.beans.prototype.bean.Prototype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Profile({"targetClass"})
public class TargetClassScheduler {

    private final Prototype prototype;


    @Async
    @Scheduled(cron = "*/5 * * * * *")
    public void invokePrototype(){
        System.out.println(this.getClass().getSimpleName());
        prototype.doJob();
        prototype.doJob();
    }

    @Autowired
    public TargetClassScheduler(Prototype prototype) {
        this.prototype = prototype;
    }
}
