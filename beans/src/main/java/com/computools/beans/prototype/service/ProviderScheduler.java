package com.computools.beans.prototype.service;

import com.computools.beans.prototype.bean.Prototype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import javax.inject.Provider;

@Service
@Profile("provider")
public class ProviderScheduler {
    private final Provider<Prototype> prototypeProvider;

    @Async
    @Scheduled(cron = "*/5 * * * * *")
    public void invokePrototype(){
        System.out.println(this.getClass().getSimpleName());
        getPrototype().doJob();
        getPrototype().doJob();
    }

    private Prototype getPrototype() {
        return prototypeProvider.get();
    }

    @Autowired
    public ProviderScheduler(Provider<Prototype> prototypeProvider) {
        this.prototypeProvider = prototypeProvider;
    }
}
