package com.computools.beans.prototype.service;

import com.computools.beans.prototype.bean.Prototype;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Profile("contextListener")
public class ContextListenerScheduler implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Async
    @Scheduled(cron = "*/5 * * * * *")
    public void invokePrototype(){
        System.out.println(this.getClass().getSimpleName());
        getPrototypeBean().doJob();
        getPrototypeBean().doJob();
    }

    private Prototype getPrototypeBean(){
        return applicationContext.getBean(Prototype.class);
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
