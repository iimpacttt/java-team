package com.computools.beans.prototype.service;

import com.computools.beans.prototype.bean.Prototype;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Profile("lookUp")
public class LookUpScheduler {


    @Scheduled(fixedDelay = 5000)
    public void prototypeJob(){
        System.out.println(this.getClass().getSimpleName());
        getPrototype().doJob();
        getPrototype().doJob();
    }

    @Lookup
    public Prototype getPrototype(){
        return null;
    }

}
