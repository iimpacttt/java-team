package com.computools.beans.prototype.service;

import com.computools.beans.prototype.bean.Prototype;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Profile("factory")
public class ObjectFactoryScheduler {
    private final ObjectFactory<Prototype> prototypeObjectFactory;

    @Async
    @Scheduled(cron = "*/5 * * * * *")
    public void invokePrototype(){
        System.out.println(this.getClass().getSimpleName());
        getPrototype().doJob();
        getPrototype().doJob();
    }

    private Prototype getPrototype() {
        return prototypeObjectFactory.getObject();
    }

    @Autowired
    public ObjectFactoryScheduler(ObjectFactory<Prototype> prototypeObjectFactory) {
        this.prototypeObjectFactory = prototypeObjectFactory;
    }
}
