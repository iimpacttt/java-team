package com.computools.beans.prototype.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE
//        , proxyMode = ScopedProxyMode.TARGET_CLASS
)
public class Prototype {
    public void doJob(){
        System.out.println();
        System.out.println(String.format("Prototype working, object name -> %s ", this.toString()));
    }

    public Prototype(){
        System.out.println();
        System.out.println("Prototype was created -> to string -> " + this.toString());
    }
}
