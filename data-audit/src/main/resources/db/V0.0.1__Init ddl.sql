CREATE TABLE if not exists comp_user
(
 id                 UUID primary key default gen_random_uuid(),
 created_time        TIMESTAMP without time zone not null ,
 updated_time       TIMESTAMP without time zone not null ,
 name               VARCHAR (255),
 password           VARCHAR (255),
 email              VARCHAR (255) unique,
 city               VARCHAR (255),
 roles              VARCHAR (355),
 gender             VARCHAR (255)
);

create index on comp_user(name);

CREATE table if not exists cell_phone(
 id                 UUID primary key default gen_random_uuid(),
 created_time       TIMESTAMP without time zone not null ,
 updated_time       TIMESTAMP without time zone not null ,
 model              varchar (255),
 manufactured_by    varchar (255),
 owner              UUID not null references comp_user(id),
 created_by         UUID not null references comp_user(id),
 updated_by         UUID not null references comp_user(id)
);

create index on cell_phone(model);

create table if not exists sim_card(
 id                 UUID primary key default gen_random_uuid(),
 created_time        TIMESTAMP without time zone not null ,
 updated_time       TIMESTAMP without time zone not null ,
 number             varchar (255) unique ,
 operator           varchar (255)
);

create table if not exists phone_sim(
 id                 UUID primary key default gen_random_uuid(),
 phone_id           UUID not null references cell_phone,
 sim_id             UUID not null references sim_card
);