package com.mvv.data.audit.dao;

import com.mvv.data.audit.model.SimCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SimCardRepo extends JpaRepository<SimCard, UUID> {
}
