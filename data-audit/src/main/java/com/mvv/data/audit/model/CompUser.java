package com.mvv.data.audit.model;

import com.mvv.data.audit.converter.Set2StringConverter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "comp_user", indexes = {@Index(columnList = "name")})
public class CompUser extends BaseEntity {

    @Column(unique = true)
    private String email;

    private String password;

    @Column(unique = true)
    private String name;

    @Column
    private String city;

    @Column
    private String gender;

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(mappedBy = "owner")
    private List<CellPhone> phones;

    @Convert(converter = Set2StringConverter.class)
    private Set<String> roles = new HashSet<>(Collections.singleton("ROLE_USER"));


    public Set<String> getRoles() {
        return roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public List<CellPhone> getPhones() {
        return phones;
    }

    public void setPhones(List<CellPhone> phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
