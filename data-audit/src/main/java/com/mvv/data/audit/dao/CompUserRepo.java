package com.mvv.data.audit.dao;

import com.mvv.data.audit.model.CompUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CompUserRepo extends JpaRepository<CompUser, UUID> {
    Optional<CompUser> getByEmail(String email);
}
