package com.mvv.data.audit.model;

import com.mvv.data.audit.audit.ComputoolsAuditor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "cell_phone", indexes = {@Index( columnList = "model")})
@EntityListeners(ComputoolsAuditor.class)
public class CellPhone extends BaseEntity {

    private String model;

    private String manufacturedBy;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner")
//    @CreatedBy
    private CompUser owner;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by")
//    @CreatedBy
    private CompUser createdBy;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "updated_by")
    @LastModifiedBy
    private CompUser updatedBy;

    @LazyCollection(LazyCollectionOption.TRUE)
    @ManyToMany
    @JoinTable(name = "phone_sim",
            joinColumns = @JoinColumn(name = "phone_id"),
            inverseJoinColumns = @JoinColumn(name = "sim_id"))
    private Set<SimCard> sims;

    public Set<SimCard> getSims() {
        return sims;
    }

    public void setSims(Set<SimCard> sims) {
        this.sims = sims;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturedBy() {
        return manufacturedBy;
    }

    public void setManufacturedBy(String manufacturedBy) {
        this.manufacturedBy = manufacturedBy;
    }

    public CompUser getOwner() {
        return owner;
    }

    public void setOwner(CompUser owner) {
        this.owner = owner;
    }

    public CompUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CompUser createdBy) {
        this.createdBy = createdBy;
    }

    public CompUser getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(CompUser updatedBy) {
        this.updatedBy = updatedBy;
    }
}
