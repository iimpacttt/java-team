package com.mvv.data.audit.dao;

import com.mvv.data.audit.model.CellPhone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CellPhonesRepo extends JpaRepository<CellPhone, UUID> {
}
