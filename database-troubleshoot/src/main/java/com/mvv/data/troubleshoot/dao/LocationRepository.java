package com.mvv.data.troubleshoot.dao;

import com.mvv.data.troubleshoot.domain.Location;
import org.geolatte.geom.Point;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    @Query(nativeQuery = true,
    value = "select * from location where name like :search")
    Page<Location> findByNameLike(@Param("search") String search, Pageable pageable);

    @Query(nativeQuery = true,
    value = "select * from location where ST_DWithin(:point, location.geo_point, :radius) order by ST_Distance(:point, location.geo_point)")
    Page<Location> findInRadius(@Param("point") Point point, @Param("radius") Long radius, Pageable pageable);

    @EntityGraph(attributePaths = {"medias"
//            , "categories"
    })
    Optional<Location> findOneWithMediaById(Long aLong);
}
