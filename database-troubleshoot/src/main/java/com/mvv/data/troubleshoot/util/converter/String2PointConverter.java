package com.mvv.data.troubleshoot.util.converter;

import org.geolatte.geom.Point;
import org.geolatte.geom.codec.Wkt;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.geolatte.geom.crs.ProjectedCoordinateReferenceSystem;
import org.geolatte.geom.crs.Unit;
import org.springframework.core.convert.converter.Converter;

import java.util.regex.Pattern;

public class String2PointConverter implements Converter<String, Point> {
    private static final ProjectedCoordinateReferenceSystem DEFAULT_REFERENCE_SYSTEM = CoordinateReferenceSystems.mkProjected(Unit.METER);
    private Pattern wktPattern = Pattern.compile("POINT\\(-?\\d+(\\.?\\d+)? -?\\d+(\\.?\\d+)?\\)", Pattern.CASE_INSENSITIVE);
    private Pattern simplePattern = Pattern.compile("-?\\d+(\\.?\\d+)?:-?\\d+(\\.?\\d+)?");

    @Override
    public Point convert(String source) {
        if (isWellKnownFormat(source)) {
            return fromWellKnownFormat(source);
        } else if (isSimpleFormat(source)) {
            return fromSimpleFormat(source);
        }
        throw new UnsupportedFormatException(source);
    }

    private boolean isSimpleFormat(String source) {
        return simplePattern.matcher(source).matches();
    }

    private boolean isWellKnownFormat(String source) {
        return wktPattern.matcher(source).matches();
    }

    private Point fromSimpleFormat(String source) {
        return fromWellKnownFormat("POINT(" + source.replace(":", " ") + ")");
    }

    private Point fromWellKnownFormat(String source) {
        return (Point) Wkt.fromWkt(source);
    }

    public static class UnsupportedFormatException extends RuntimeException {
        public UnsupportedFormatException(String message) {
            super("Unsupported point format " + message);
        }
    }
}

