package com.mvv.data.troubleshoot.domain;

import com.mvv.data.troubleshoot.domain.converter.RoleSet2StringConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
@Entity
@Table(name = "account")
public class Account extends AbstractEntity {
    private static final long serialVersionUID = 6122008236542488010L;

    @Column(name = "firebase_uid", unique = true, nullable = false)
    private String firebaseUid;

    private String nickname;

    @Column(name = "profile_image")
    private String profileImage;

    @Convert(converter = RoleSet2StringConverter.class)
    private Set<AccountRole> roles = new LinkedHashSet<>();

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "location_owners",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "location_id"))
    private Set<Location> locations = new LinkedHashSet<>();

    private LocalDateTime lastUpdated;

    @Column(nullable = false, updatable = false)
    private LocalDateTime creation;

    @Transient
    private long points;

    public long getPoints() {
        return points;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    public String getFirebaseUid() {
        return firebaseUid;
    }

    public void setFirebaseUid(String firebaseUid) {
        this.firebaseUid = firebaseUid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getTimestamp() {
        return lastUpdated;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.lastUpdated = timestamp;
    }

    public Set<AccountRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AccountRole> roles) {
        this.roles = roles;
    }

    public void addLocations(Collection<Location> locations) {
        if (this.locations != null) {
            this.locations.addAll(locations);
        }
    }

    public void addRole(AccountRole role) {
        if (roles == null) {
            roles = new LinkedHashSet<>();
        }

        roles.add(role);
    }

    public void removeRole(AccountRole role) {
        if (roles == null) {
            roles = new LinkedHashSet<>();
        }

        roles.remove(role);
    }

    public void addLocation(Location location) {
        if (locations == null) {
            locations = new LinkedHashSet<>();
        }

        locations.add(location);
    }

    public void removeLocation(Location location) {
        if (locations == null) {
            locations = new LinkedHashSet<>();
        }

        locations.remove(location);
    }


}

