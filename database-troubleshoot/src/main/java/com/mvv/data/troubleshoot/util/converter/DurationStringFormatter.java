package com.mvv.data.troubleshoot.util.converter;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;

import java.text.ParseException;
import java.time.Duration;
import java.util.Locale;

public class DurationStringFormatter implements Formatter<Duration>, ApplicationContextAware {
    public DurationStringFormatter() {
    }

    @Override
    public Duration parse(String s, Locale locale) throws ParseException {
        return Duration.parse(s);
    }

    @Override
    public String print(Duration duration, Locale locale) {
        return duration.toString();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        applicationContext
                .getBeansOfType(FormatterRegistry.class)
                .values()
                .forEach(r -> r.addFormatter(this));
    }
}