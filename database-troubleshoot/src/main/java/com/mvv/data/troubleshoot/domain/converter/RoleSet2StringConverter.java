package com.mvv.data.troubleshoot.domain.converter;

import com.mvv.data.troubleshoot.domain.AccountRole;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.persistence.Converter;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

@Converter
public class RoleSet2StringConverter extends JsonAttributeConverter<Set> {
    public RoleSet2StringConverter() {
        super(LinkedHashSet.class, LinkedHashSet::new);
    }

    @Override
    protected Set fromDataBase(String dbData) {
        try {
            return getObjectMapper().readValue(dbData, new TypeReference<LinkedHashSet<AccountRole>>(){});
        } catch (IOException | ObjectMapperHolder.ObjectMapperHolderNotInitialized e) {
            throw new RuntimeException(e);
        }
    }
}
