package com.mvv.data.troubleshoot.config;

import com.mvv.data.troubleshoot.domain.converter.ObjectMapperHolder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.geolatte.geom.json.Feature;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

@Configuration
@EnableAutoConfiguration
@Import({DataConfig.class, ServiceConfig.class, WebConfig.class})
public class DataTroubleShootConfig {

    @Bean
    @Primary
    public ObjectMapper objectMapper() {

        return new ObjectMapper()
                .registerModule(new GeolatteGeomModule())
                .registerModule(new JavaTimeModule())
                .registerModule(new Hibernate5Module())   //TODO THIS IS IMPORTANT
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                ;
    }

    @Bean
    public ObjectMapperHolder objectMapperHolder(ObjectMapper objectMapper) {
        return new ObjectMapperHolder(objectMapper);
    }

    @Bean
    public GeolatteGeomModule geolatteGeomModule() {
        GeolatteGeomModule geolatteGeomModule = new GeolatteGeomModule();
        geolatteGeomModule.setFeature(Feature.SUPPRESS_CRS_SERIALIZATION, true);

        return geolatteGeomModule;
    }


}
