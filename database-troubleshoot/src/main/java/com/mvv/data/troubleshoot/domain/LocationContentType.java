package com.mvv.data.troubleshoot.domain;

public enum LocationContentType {
    HTML, PDF, IMAGE
}
