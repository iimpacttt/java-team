package com.mvv.data.troubleshoot.domain;

public enum Language {
    EN, FR
}
