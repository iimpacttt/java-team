package com.mvv.data.troubleshoot.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "close_period")
public class ClosePeriod extends AbstractEntity{
    private static final long serialVersionUID = 5600677455705685618L;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToOne
    @JoinColumn(nullable = false)
    private LocationWorkSchedule locationWorkSchedule;

    @Column(nullable = false)
    private LocalDateTime startDate;


    private String comment;


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocationWorkSchedule getLocationWorkSchedule() {
        return locationWorkSchedule;
    }

    public void setLocationWorkSchedule(LocationWorkSchedule locationWorkSchedule) {
        this.locationWorkSchedule = locationWorkSchedule;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

}
