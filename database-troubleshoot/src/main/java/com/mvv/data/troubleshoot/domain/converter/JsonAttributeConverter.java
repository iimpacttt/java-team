package com.mvv.data.troubleshoot.domain.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

public abstract class JsonAttributeConverter<T> extends Abstract2StringAttributeConverter<T> {
    protected final Class<? extends T> attributeClass;
    protected volatile ObjectMapper objectMapper;

    @SuppressWarnings("unchecked")
    public JsonAttributeConverter(Class<? extends T> attributeClass) {
        this(attributeClass, (Supplier<T>) DEFAULT_NULL_RESOLVER);
    }

    public JsonAttributeConverter(Class<? extends T> attributeClass, Supplier<T> nullResolver) {
        super(nullResolver);
        this.attributeClass = requireNonNull(attributeClass);
    }

    @Override
    protected String toDataBase(T attribute) {
        try {
            return getObjectMapper().writeValueAsString(attribute);
        } catch (IOException | ObjectMapperHolder.ObjectMapperHolderNotInitialized e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected T fromDataBase(String dbData) {
        try {
            return getObjectMapper().readValue(dbData, attributeClass);
        } catch (IOException | ObjectMapperHolder.ObjectMapperHolderNotInitialized e) {
            throw new RuntimeException(e);
        }
    }

    public ObjectMapper getObjectMapper() throws ObjectMapperHolder.ObjectMapperHolderNotInitialized {
        if (objectMapper == null) {
            synchronized (this) {
                if (objectMapper == null) {
                    objectMapper = ObjectMapperHolder.getObjectMapper();
                }
            }
        }

        return objectMapper;
    }

    @Override
    protected boolean isEmptyDbData(String dbData) {
        return StringUtils.isBlank(dbData);
    }
}