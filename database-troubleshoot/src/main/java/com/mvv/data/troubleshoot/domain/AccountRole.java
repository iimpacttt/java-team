package com.mvv.data.troubleshoot.domain;

public enum AccountRole
//        implements GrantedAuthority THIS IS ONLY FOR SECURITY, WE DO NOT NEED THIS FOR OUR TESTS
{
    ROLE_DEFAULT,
    ROLE_TRIPPIN_PRO,
    ROLE_TRIPPIN_DIRECTOR,
    ROLE_ADMIN;

//    @Override
//    public String getAuthority() {
//        return name();
//    }
}
