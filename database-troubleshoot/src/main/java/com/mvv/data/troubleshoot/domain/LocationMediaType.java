package com.mvv.data.troubleshoot.domain;

public enum LocationMediaType {
    PDF, YOUTUBE, IMAGE
}
