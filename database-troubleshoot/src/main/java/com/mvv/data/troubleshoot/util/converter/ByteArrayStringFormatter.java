package com.mvv.data.troubleshoot.util.converter;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class ByteArrayStringFormatter implements Formatter<byte[]>, ApplicationContextAware {
    @Override
    public byte[] parse(String s, Locale locale) {
        return s.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String print(byte[] bytes, Locale locale) {
        return new String(bytes, StandardCharsets.UTF_8);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        applicationContext
                .getBeansOfType(FormatterRegistry.class)
                .values()
                .forEach(r -> r.addFormatter(this));
    }
}