package com.mvv.data.troubleshoot.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "location_media")
@NamedNativeQueries({
        @NamedNativeQuery(name = "Media.getMediaLike",
                query = "SELECT * FROM location_media WHERE url like :patern limit :limit",
                resultClass = Media.class)
})
public class Media extends AbstractEntity {
    private static final long serialVersionUID = 5425672456079794398L;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private LocationMediaType type;

    @Column(nullable = false)
    private String url;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_id",nullable = false)
    @JsonIgnore
    private Location location;

    @Column(name = "sort_order",nullable = false)
    private Integer sortOrder = 0;

    private LocalDateTime lastUpdated;

    @Column(nullable = false, updatable = false)
    private LocalDateTime creation;

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getTimestamp() {
        return lastUpdated;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.lastUpdated = timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setType(LocationMediaType type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Media media = (Media) o;

        if (type != null ? !type.equals(media.type) : media.type != null) return false;
        if (url != null ? !url.equals(media.url) : media.url != null) return false;
        if (location != null ? !location.equals(media.location) : media.location != null) return false;
        if (sortOrder != null ? !sortOrder.equals(media.sortOrder) : media.sortOrder != null) return false;
        if (lastUpdated != null ? !lastUpdated.equals(media.lastUpdated) : media.lastUpdated != null) return false;
        return creation != null ? creation.equals(media.creation) : media.creation == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (sortOrder != null ? sortOrder.hashCode() : 0);
        result = 31 * result + (lastUpdated != null ? lastUpdated.hashCode() : 0);
        result = 31 * result + (creation != null ? creation.hashCode() : 0);
        return result;
    }
}

