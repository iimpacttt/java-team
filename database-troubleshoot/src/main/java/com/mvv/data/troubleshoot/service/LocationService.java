package com.mvv.data.troubleshoot.service;

import com.mvv.data.troubleshoot.dao.LocationRepository;
import com.mvv.data.troubleshoot.domain.Location;
import com.mvv.data.troubleshoot.util.converter.String2PointConverter;
import org.geolatte.geom.Point;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LocationService {
    private final LocationRepository locationRepository;
    private final String2PointConverter converter = new String2PointConverter();

    public Location getById(Long id) {
        return locationRepository.findById(id).orElseThrow(() -> new RuntimeException("Entity not found"));
    }

    public Page<Location> findByText(String search, Pageable pageable) {
        return locationRepository.findByNameLike("%" + search + "%", pageable);
    }

    public Page<Location> findInRadius(String  stringPoint, Long radius, Pageable pageable) {
        Point point = converter.convert(stringPoint);
        return locationRepository.findInRadius(point, radius, pageable);
    }

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public Location findOneWithMedia(Long id) {
        return locationRepository.findOneWithMediaById(id).orElseThrow(() -> new RuntimeException("Entity not found"));
    }
}
