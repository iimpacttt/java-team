package com.mvv.data.troubleshoot;

import com.mvv.data.troubleshoot.config.DataTroubleShootConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;

public class DataTroubleShootStarter {
    public static void main(String[] args) {
        new SpringApplicationBuilder(DataTroubleShootConfig.class).run(args);
    }
}
