package com.mvv.data.troubleshoot.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "location_content")
public class LocationContent extends AbstractNamedEntity {
    private static final long serialVersionUID = -6574494001522073556L;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private LocationContentType type;

    @Column(nullable = false)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location_id")
    @JsonIgnore
    private Location location;

    @Column(name = "sort_order", nullable = false)
    private Integer sortOrder = 0;

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public LocationContentType getType() {
        return type;
    }

    public void setType(LocationContentType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
