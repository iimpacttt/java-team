package com.mvv.data.troubleshoot.domain;


import com.mvv.data.troubleshoot.domain.converter.Address2StringConverter;
import com.mvv.data.troubleshoot.domain.converter.Set2StringConverter;
import com.mvv.data.troubleshoot.domain.converter.ZoneId2StringConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.geolatte.geom.Point;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "location")
public class Location extends AbstractNamedEntity {

    private static final long serialVersionUID = -2466678492285637130L;
    private static final Language DEFAULT_LANGUAGE = Language.EN;
    private static final ZoneOffset DEFAULT_TIME_ZONE = ZoneOffset.UTC;

    private String description;
    @Column(name = "geo_point", columnDefinition = "GEOGRAPHY(Point)", nullable = false)
    private Point geoPoint;

    @Convert(converter = Set2StringConverter.class)
    private Set<String> phones = new LinkedHashSet<>();

    @Column
    private String email;

    @Column
    private String website;

    @Convert(converter = Address2StringConverter.class)
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "last_update_by")
    @JsonIgnore
    private Account lastUpdatedBy;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "location_tag",
            joinColumns = @JoinColumn(
                    name = "location_id", referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "tag_id"
            )
    )
    private Set<Tag> tags = new LinkedHashSet<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "location_category",
            joinColumns = @JoinColumn(name = "location_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> categories = new LinkedHashSet<>();

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Media> medias = new ArrayList<>();

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LocationContent> locationContents = new ArrayList<>();

    @OneToMany(mappedBy = "location", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<LocationWorkSchedule> workSchedule = new ArrayList<>();

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    @Column(nullable = false, updatable = false)
    private LocalDateTime creation;

    @Column(name = "needs_maintenance")
    private Boolean needsMaintenance = false;

    @Enumerated(EnumType.STRING)
    private Language language = DEFAULT_LANGUAGE;

    @Convert(converter = ZoneId2StringConverter.class)
    private ZoneId timeZone = DEFAULT_TIME_ZONE;


    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getPhones() {
        return phones;
    }

    public void setPhones(Set<String> phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Account getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Account lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdated) {
        this.lastUpdate = lastUpdated;
    }

    public LocalDateTime getTimestamp() {
        return lastUpdate;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.lastUpdate = timestamp;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    public List<LocationContent> getLocationContents() {
        return locationContents;
    }

    public void setLocationContents(List<LocationContent> locationContents) {
        this.locationContents = locationContents;
    }

    public List<LocationWorkSchedule> getWorkSchedule() {
        return workSchedule;
    }

    public void setWorkSchedule(List<LocationWorkSchedule> workSchedule) {
        this.workSchedule = workSchedule;
    }

    public Boolean getNeedsMaintenance() {
        return needsMaintenance;
    }

    public void setNeedsMaintenance(Boolean needsMaintenance) {
        this.needsMaintenance = needsMaintenance;
    }

    public ZoneId getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(ZoneId timeZone) {
        this.timeZone = timeZone;
    }


    public Point getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(Point geoPoint) {
        this.geoPoint = geoPoint;
    }


    public Location() {
    }

    public Location(Long id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

}

