package com.mvv.data.troubleshoot.domain.converter;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.Converter;
import java.util.LinkedHashSet;
import java.util.Set;

@Converter
public class Set2StringConverter extends JsonAttributeConverter<Set> {
    public Set2StringConverter() {
        super(LinkedHashSet.class, LinkedHashSet::new);
    }

    //for unit testing
    Set2StringConverter(ObjectMapper objectMapper) {
        this();
        this.objectMapper = objectMapper;
    }
}
