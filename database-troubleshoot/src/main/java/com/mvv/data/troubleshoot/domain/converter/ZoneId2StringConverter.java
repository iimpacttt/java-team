package com.mvv.data.troubleshoot.domain.converter;

import org.springframework.data.convert.Jsr310Converters;

import javax.persistence.Converter;
import java.time.ZoneId;

@Converter(autoApply = true)
public class ZoneId2StringConverter extends Abstract2StringAttributeConverter<ZoneId> {
    @Override
    public String toDataBase(ZoneId attribute) {
        return Jsr310Converters.ZoneIdToStringConverter.INSTANCE.convert(attribute);
    }

    @Override
    public ZoneId fromDataBase(String dbData) {
        return Jsr310Converters.StringToZoneIdConverter.INSTANCE.convert(dbData);
    }
}
