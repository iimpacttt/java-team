package com.mvv.data.troubleshoot.domain.converter;

import com.mvv.data.troubleshoot.domain.Address;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Address2StringConverter extends JsonAttributeConverter<Address> {
    public Address2StringConverter() {
        super(Address.class);
    }

    //for unit testing
    Address2StringConverter(ObjectMapper objectMapper) {
        this();
        this.objectMapper = objectMapper;
    }
}
