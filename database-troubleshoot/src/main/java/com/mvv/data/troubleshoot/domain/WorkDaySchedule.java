package com.mvv.data.troubleshoot.domain;

import java.io.Serializable;
import java.time.DayOfWeek;

public class WorkDaySchedule implements Serializable {
    private static final long serialVersionUID = -8087923912040320344L;

    private DayOfWeek dayOfWeek;

    private Integer startHour;

    private Integer startMinute;

    private Integer closeHour;

    private Integer closeMinute;

    private Boolean closed;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public void setStartHour(Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Integer getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(Integer closeHour) {
        this.closeHour = closeHour;
    }

    public Integer getCloseMinute() {
        return closeMinute;
    }

    public void setCloseMinute(Integer closeMinute) {
        this.closeMinute = closeMinute;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkDaySchedule that = (WorkDaySchedule) o;

        if (dayOfWeek != that.dayOfWeek) return false;
        if (startHour != null ? !startHour.equals(that.startHour) : that.startHour != null) return false;
        if (startMinute != null ? !startMinute.equals(that.startMinute) : that.startMinute != null) return false;
        if (closeHour != null ? !closeHour.equals(that.closeHour) : that.closeHour != null) return false;
        if (closeMinute != null ? !closeMinute.equals(that.closeMinute) : that.closeMinute != null) return false;
        return closed != null ? closed.equals(that.closed) : that.closed == null;
    }

    @Override
    public int hashCode() {
        int result = dayOfWeek != null ? dayOfWeek.hashCode() : 0;
        result = 31 * result + (startHour != null ? startHour.hashCode() : 0);
        result = 31 * result + (startMinute != null ? startMinute.hashCode() : 0);
        result = 31 * result + (closeHour != null ? closeHour.hashCode() : 0);
        result = 31 * result + (closeMinute != null ? closeMinute.hashCode() : 0);
        result = 31 * result + (closed != null ? closed.hashCode() : 0);
        return result;
    }
}
