package com.mvv.data.troubleshoot.web;

import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class WebAdvice {
    private class ErrorResponse {
        private final String message = "This is my handler";
    }

//    @ExceptionHandler(RuntimeException.class)
//    public ResponseEntity<ErrorResponse> handle(RuntimeException e){
//        return new ResponseEntity<>(new ErrorResponse(), HttpStatus.NOT_FOUND);
//    }
}
