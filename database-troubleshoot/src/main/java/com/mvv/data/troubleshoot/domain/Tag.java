package com.mvv.data.troubleshoot.domain;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "tag")
@NamedNativeQueries({
        @NamedNativeQuery(name = "tag.getTagsListByCategory",
                resultClass = Tag.class,
                query = "select * from tag where id in (select tag_id from tag_category where category_id = :categoryId) limit :limit offset :offset")
})
public class Tag extends AbstractNamedEntity {

    private static final long serialVersionUID = 8464944521486155418L;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "tag_category",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> categories = new LinkedHashSet<>();

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private LocalDateTime creation;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

}
