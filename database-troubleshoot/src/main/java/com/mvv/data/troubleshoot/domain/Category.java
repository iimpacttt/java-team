package com.mvv.data.troubleshoot.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "category", indexes = @Index(columnList = "name"))
public class Category extends AbstractNamedEntity  {
    private static final long serialVersionUID = 358165717220252589L;

    @Enumerated(EnumType.STRING)
    private CategoryType type;

    @Column(nullable = false, updatable = false)
    private LocalDateTime creation;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tag_category",
            joinColumns = @JoinColumn(
                    name = "category_id", referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "tag_id"
            )
    )
    private Set<Tag> tags = new LinkedHashSet<>();

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public Category(Long id) {
        this.id = id;
    }

    public Category() {
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

}
