package com.mvv.data.troubleshoot.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.computools.data.troubleshoot.web"})
public class WebConfig {

}
