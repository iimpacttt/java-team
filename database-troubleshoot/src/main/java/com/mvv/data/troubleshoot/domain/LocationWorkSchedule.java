package com.mvv.data.troubleshoot.domain;

import com.mvv.data.troubleshoot.domain.converter.Set2StringConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "location_work_schedule")
public class LocationWorkSchedule extends AbstractEntity {
    private static final long serialVersionUID = -7679982469380396769L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "location_id")
    @JsonIgnore
    private Location location;

    @OneToMany(mappedBy = "locationWorkSchedule", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ClosePeriod> closePeriods = new LinkedHashSet<>();

    @Convert(converter = Set2StringConverter.class)
    @Column(columnDefinition = "text", nullable = false)
    private Set<WorkDaySchedule> workDays = new LinkedHashSet<>();

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<WorkDaySchedule> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(Set<WorkDaySchedule> workDays) {
        this.workDays = workDays;
    }

    public Set<ClosePeriod> getClosePeriods() {
        return closePeriods;
    }

    public void setClosePeriods(Set<ClosePeriod> closePeriods) {
        this.closePeriods = closePeriods;
    }

}
