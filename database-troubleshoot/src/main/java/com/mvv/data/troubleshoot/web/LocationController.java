package com.mvv.data.troubleshoot.web;

import com.mvv.data.troubleshoot.domain.Location;
import com.mvv.data.troubleshoot.service.LocationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/location")
public class LocationController {

    private final LocationService locationService;

    @GetMapping("/{id}")
    public ResponseEntity<Location> getById(@PathVariable Long id){
        return ResponseEntity.ok(locationService.getById(id));
    }

    @GetMapping("/search")
    public ResponseEntity<Page<Location>> searchLocations(@RequestParam String search,
                                                          Pageable pageable){
        return ResponseEntity.ok(locationService.findByText(search, pageable));
    }

    @GetMapping("/geoSearch")
    public ResponseEntity<Page<Location>> geoSearchLocations(@RequestParam String point,
                                                             @RequestParam Long radius,
                                                             Pageable pageable){
        Page<Location> result = locationService.findInRadius(point, radius, pageable);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}/media")
    public ResponseEntity<Location> getWithMedia(@PathVariable Long id){
        return ResponseEntity.ok(locationService.findOneWithMedia(id));
    }


    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }
}
